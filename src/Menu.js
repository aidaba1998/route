import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';

import {Link as NavLink,Outlet} from 'react-router-dom';



function Menu() {
    return (
        <div>
        <nav className ="navbar navbar-expand-lg navbar-light bg-primary">
  <div className ="container-fluid">
    
    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span className ="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarNav">
      <ul className ="navbar-nav ">
        <li className ="nav-item">
          <NavLink className ="nav-link " aria-current="page" to="/">Home</NavLink>
        </li>
        <li className="nav-item ">
          <NavLink className ="nav-link " to="Acceuil">Acceuil</NavLink>
        </li>
        <li className="nav-item">
          <NavLink className ="nav-link" to="About">About</NavLink>
        </li>
       
      </ul>
    </div>
  </div>
</nav>
<Outlet/>
          
        </div>
    )
}

export default Menu
