import React from 'react'

import Home from './Home';
import Acceuil from './Acceuil';
import About from './About';
import Menu from './Menu';

import {Routes,Route} from 'react-router-dom';

function App() {
 
  return (
    <div style={{textAlign:'center',display:'inline'}} >
       
      <Routes>
        <Route path='/' element={ <Menu/>}>
          <Route index element={ <Home/>}/>
          <Route path='About' element={ <About/>}/>
          <Route path='Acceuil' element={ <Acceuil/>}/>
        </Route>
      </Routes>
    
   
      
    </div>
  )
}

export default App
